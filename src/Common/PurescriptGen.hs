module Common.PurescriptGen where

import ClassyPrelude

import Data.Proxy                          (Proxy (..))
import Language.PureScript.Bridge          (SumType, buildBridge, defaultBridge, doCheck,
                                            haskType, mkTypeInfo, psTypeParameters,
                                            typeParameters, writePSTypes)
import Language.PureScript.Bridge.Builder  (BridgePart, (^==))
import Language.PureScript.Bridge.TypeInfo (Language (..), TypeInfo (..), typeName)
import Lens.Micro.Platform                 ((^.))


exportPureScriptTypes :: BridgePart -> [SumType 'Haskell] -> FilePath -> IO ()
exportPureScriptTypes bridge types dir = writePSTypes dir (buildBridge bridge) types

poomasBridge :: BridgePart
poomasBridge = stringBridge
           <|> mapHsToPsType "Text"    "String"  ""
           <|> mapHsToPsType "Map"     "Map"     "Data.Map"
           <|> mapHsToPsType "Set"     "Set"     "Data.Set"
           <|> mapHsToPsType "[]"      "Array"   ""
           <|> mapHsToPsType "UTCTime" "UTCTime" "Type.UTCTime"
           <|> mapHsToPsType "UUID"    "UUID"    "Data.UUID"
           <|> mapHsToPsType "Word8"   "Int"     ""
           <|> mapHsToPsType "Value"   "JSON"    "Type.JSON"
           <|> valueBridge
           <|> objectBridge
           <|> defaultBridge

  where
    mapHsToPsType hsType psType moduleName = do
      typeName ^== hsType
      TypeInfo "" moduleName psType <$> psTypeParameters
    stringBridge = do
      haskType ^== mkTypeInfo (Proxy :: Proxy String )
      return $ TypeInfo "" "" "String" []


baseUrlBridge :: Text -> BridgePart
baseUrlBridge moduleFile = do
  typeName ^== "BaseUrl"
  return TypeInfo
    { _typePackage    = ""
    , _typeModule     = moduleFile
    , _typeName       = "BaseUrl"
    , _typeParameters = []
    }

valueBridge :: BridgePart
valueBridge = do
   typeName ^== "Value"
   pure TypeInfo
      { _typePackage    = ""
      , _typeModule     = "Type.JSON"
      , _typeName       = "JSON"
      , _typeParameters = []
      }

objectBridge :: BridgePart
objectBridge = do
  typeName ^== "HashMap"
  doCheck typeParameters isObject
  return TypeInfo
    { _typePackage    = ""
    , _typeModule     = "Type.JSON"
    , _typeName       = "JSON"
    , _typeParameters = []
    }

  where
    isObject :: [TypeInfo lang] -> Bool
    isObject [k,v] = k ^. typeName == "Text" && v ^. typeName == "Value"
    isObject _     = False

setModuleName :: Text -> Text -> Text -> BridgePart
setModuleName dir hsType modulePath = do
   typeName ^== hsType
   pure TypeInfo
      { _typePackage    = ""
      , _typeModule     = dir <> modulePath
      , _typeName       = hsType
      , _typeParameters = []
      }
